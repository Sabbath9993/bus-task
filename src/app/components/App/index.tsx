import React, {useState, useEffect, useCallback} from 'react';

import Modal from "app/components/Modal";

import minutesToNextBus from "app/lib/minutesToNextBus/minutesToNextBus";

import busImg from './images/school-bus.png';

import cx from 'classnames';

import styles from './styles.module.scss';

interface IAppPropTypes {}

const App:React.FC<IAppPropTypes> = () => {

    const [isOpen, setIsOpen] = useState<boolean>(false);

    const [lastMoment, setLastMoment] = useState<number>(0);

    const onClose = useCallback(() => {
        setIsOpen(false)
    }, [])

    useEffect(() => {
        let timer = setInterval(() => {
            setLastMoment(minutesToNextBus(new Date().getHours(), new Date().getMinutes()))
    }, 1000)

        return () => {
            clearInterval(timer)
        }
    }, [])

    return (
        <div className={cx(styles.root)}>
            <div className={cx(styles.container)}>
                    <button className={cx(styles.btn)} onClick={() => setIsOpen(!isOpen)}>check time</button>
            </div>

            <Modal isOpen={isOpen} onClose={onClose}>
                <div className={cx(styles.bus)}>
                    <img src={busImg} alt="bus"/>
                </div>

                <p>You should go to bus station in:</p>

                <p className={cx(styles.number)}>{ lastMoment }</p>

                <p>minutes</p>
            </Modal>
        </div>
    )
}

export default App;