import React from 'react';

import { createPortal} from "react-dom";

import { IWithChildren } from "app/types/IWithChildren";

import { IconCross} from "app/Icon/IconCross/index";

import cx from 'classnames';

import styles from './styles.module.scss';

interface IModalPropTypes extends IWithChildren {
    isOpen: boolean;
    onClose: () => void;
}

const Modal:React.FC<IModalPropTypes> = (props: IModalPropTypes) => {

    const { isOpen, children, onClose } = props;

    if(!isOpen) return null;

    return createPortal (
        <>
            <div onClick={onClose} className={cx(styles.root)} />
            <div className={cx(styles.modal)}>
                <button className={styles.cross} onClick={onClose}>
                    <IconCross className={cx(styles.crossIcon)} />
                </button>

                { children }
            </div>
        </>,
        document.getElementById('portal')
    )
}

export default Modal;

