const ComparatorValues = {
    GREAT  : 1,
    EQUALS : 0,
    LESS   : -1
}

const FIRST_BUS_ARRIVAL = {
    H : 6,
    M : 0
}

const LAST_BUS_ARRIVAL = {
    H : 0,
    M : 0
}

const INTERVAL_BETWEEN_BUSSES = {
    H : 0,
    M : 15
}

const MINUTES_TO_BUS = {
    H : 0,
    M : 5
}

/**
 * Shows minutes amount from home to next bus.
 *
 * @param {number} hours Hours amount
 * @param {number} minutes Minutes amount
 *
 * */

export default function minutesToNextBus(hours, minutes) {
    let currentTime = {
        H : hours,
        M : minutes
    }

    return calculateTimeToExit(currentTime);
}

function validateTime(time) {
    if (time.H > 23 || time.H < 0) {
        return false
    }

    if (time.M > 59 || time.M < 0) {
        return false
    }

    return true
}

function compareTime(time1, time2) {
    if (!validateTime(time1) || !validateTime(time2)) {
        throw new Error("Time format error");
    }

    if (time1.H > time2.H) {
        return ComparatorValues.GREAT;
    } else if (time1.H < time2.H) {
        return ComparatorValues.LESS;
    } else {
        return time1.M > time2.M ? ComparatorValues.GREAT :
            time1.M === time2.M ? ComparatorValues.EQUALS : ComparatorValues.LESS
    }
}


function timeDifference(time1, time2) {
    let isNegativeMinutesDifference = time1.M < time2.M;
    let isNegativeHoursDifference = time1.H - time2.H - (isNegativeMinutesDifference ? 1 : 0) < 0;

    return {
        H : time1.H +
            (isNegativeHoursDifference ? 24 : 0) -
            time2.H -
            (isNegativeMinutesDifference ? 1 : 0),
        M : time1.M +
            (isNegativeMinutesDifference ? 60 : 0) -
            time2.M
    }
}


function timeSum(time1, time2) {
    let minutesSum = time1.M + time2.M;

    return {
        H : (time1.H + time2.H + Math.floor(minutesSum / 60)) % 24,
        M : minutesSum % 60
    }
}


function timeToMinutes(time) {
    return time.H * 60 + time.M
}

function calculateTimeToExit (time,
                              startTime=FIRST_BUS_ARRIVAL,
                              endTime=LAST_BUS_ARRIVAL,
                              interval=INTERVAL_BETWEEN_BUSSES,
                              timeToBus=MINUTES_TO_BUS) {
    if (!validateTime(time)) {
        throw new Error('Time format error');
    }

    let outTime = null;

    if (compareTime(time, startTime) === ComparatorValues.LESS ||
        compareTime(
            timeDifference(endTime, time), // time to last bus
            timeToBus) === ComparatorValues.LESS) {
        outTime = timeDifference(timeDifference(startTime, timeToBus), time);
    } else {
        let currentTimeInMinutes = timeToMinutes(time);
        let startTimeInMinutes = timeToMinutes(startTime);
        let busIntervalInMinutes = timeToMinutes(interval);
        let minutesToBus = timeToMinutes(timeToBus);
        let minutesToFirstBus = currentTimeInMinutes - startTimeInMinutes;
        let minutesToNextBus = busIntervalInMinutes - (minutesToFirstBus % busIntervalInMinutes);

        if (minutesToNextBus === minutesToBus) {
            outTime = {H : 0, M : 0};
        } else if (minutesToNextBus > minutesToBus) {
            outTime = {
                H : Math.floor((minutesToNextBus - minutesToBus) / 60),
                M : (minutesToNextBus - minutesToBus) % 60};
        } else {
            outTime = {
                H : Math.floor(minutesToNextBus / 60),
                M : minutesToNextBus % 60};

            while (compareTime(timeSum(outTime, timeToBus), interval) === ComparatorValues.LESS) {
                outTime = timeSum(outTime, interval);
            }
            outTime = timeDifference(outTime, timeToBus);
        }
    }

    return timeToMinutes(outTime);
}