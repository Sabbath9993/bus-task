import minutesToNextBus from "../minutesToNextBus";

describe('returns time from home to bus station in minutes', () => {
    test('Should return 10', () => {
        expect(minutesToNextBus(15, 15)).toBe(10)
    })

    test('Should return 0', () => {
        expect(minutesToNextBus(23, 40)).toBe(0)
    })

    test('Should return 354', () => {
        expect(minutesToNextBus(0, 1)).toBe(354)
    })

    test('Should return 10', () => {
        expect(minutesToNextBus(6, 0)).toBe(10)
    })
})